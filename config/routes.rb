Rails.application.routes.draw do
  get "/tareas", to:"tareas#index"
  get "/tareas/:id", to:"tareas#show"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
