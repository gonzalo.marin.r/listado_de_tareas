class CreateTareas < ActiveRecord::Migration[6.0]
  def change
    create_table :tareas do |t|
      t.boolean :completed
      t.text :body

      t.timestamps
    end
  end
end
